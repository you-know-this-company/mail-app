const agenda = require('./agenda');
const mailgun = require('./mailgun');

module.exports = {
    scheduler: agenda,
    mail: mailgun
};
