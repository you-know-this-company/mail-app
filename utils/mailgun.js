const fetch = require('node-fetch');
const FormData = require('form-data');

const { MG_URL, MG_DOMAIN, MG_APIKEY, MG_FROM } = process.env;

const BASE_URL = `${MG_URL}/${MG_DOMAIN}`;

const apiKey = Buffer.from(`api:${MG_APIKEY}`).toString('base64');
const authorization = `Basic ${apiKey}`;

const sendMail = async (to = '', subject = '', body = '') => {
    const url = `${BASE_URL}/messages`;

    const form = new FormData();

    form.append('from', MG_FROM);
    form.append('to', to);
    form.append('subject', subject);
    form.append('html', body);

    const req = await fetch(url, {
        method: 'POST',
        body: form,
        headers: {
            Authorization: authorization
        }
    });

    const res = await req.json();

    return res;
};

module.exports = {
    sendMail
};
