const Agenda = require('agenda');

const mail = require('./mailgun');
const { constants } = require('../common');

const { SEND_EMAIL } = constants;

const {
    MONGODB_URI = 'mongodb://localhost:27017/send-the-mail',
    MONGODB_COLLECTION = 'jobs',
    SCHEDULER_PROCESS_EVERY = '1 minute',
    SCHEDULER_LOCK_LIMIT = 10
} = process.env;

const agenda = new Agenda({
    db: {
        address: MONGODB_URI,
        collection: MONGODB_COLLECTION
    },
    processEvery: SCHEDULER_PROCESS_EVERY,
    defaultLockLimit: parseInt(SCHEDULER_LOCK_LIMIT, 10)
});

agenda.define(SEND_EMAIL, async job => {
    const { data } = job.attrs;
    const { to, subject, content } = data;

    try {
        const res = await mail.sendMail(to, subject, content);

        const { id, message } = res;

        // Save Mailgun identifier
        if (id) {
            // eslint-disable-next-line no-param-reassign
            job.attrs.mailgun_id = id;

            await job.save();
        }

        console.info(message);
    } catch (error) {
        console.error(error);
    }
});

module.exports = agenda;
