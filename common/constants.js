const notifications = {
    SCHEDULER_STARTED: 'Scheduler started.'
};

const jobs = {
    SEND_EMAIL: 'SEND_EMAIL'
};

module.exports = {
    ...notifications,
    ...jobs
};
