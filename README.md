# Mail App

A simple mail-sending service

## Installation

```bash
npm install         # install necessary modules

npm start           # run the service
```

An `.env-sample` file is provided for configuration. Please, rename to `.env` in order to use the Mailgun Credentials inside. In a productiontion environment, we obviously wouldn't commit those.

**IMPORTANT** - the Mailgun API Credentials in `.env-sample` are for a free account with no bank card. As such the **only authorized recipients** are ... `stoyan.berov@gmail.com`. Please, send emails to him from Platform App UI.

I am actually a big fan of Mailgun and use it for personal needs, but forgive me for not committing my _real-real credentials_. Hope that's okay!

## Technical decisions

-   Recurring dates in `RFC 5545 (iCalendar)` format - discussed in more detail in Platform App README.md
-   Job Scheduling with `agenda` - discussed in more detail in Platform App README.md. It should be possible to launch the service as many times and on as many computers as you wish - `agenda` will take care to distribute the load

### General

-   Code styling is kept standardized using AirBnB eslint
-   Code formatting is kept standardized using Prettier

## Known issues & Improvements

-   Code Styling and Formatting rules come from 6-7 npm libraries, and are similar to those in Application App => in a real-world it may be worth spending some company time to publish a shared npm module
-   Consider using a more efficient queuing system - maybe RabbitMQ or something Redis-based
-   Depending on taste and OOP background, some devs may prefer to create "Mail" and "Scheduler" classes and inject "agenda" and "mailgun" into them.
-   Upgrade Mailgun Sandbox account or verify 5 emails
