require('dotenv').config();

const { scheduler } = require('./utils');
const { constants } = require('./common');

const { SCHEDULER_STARTED } = constants;

scheduler
    .start()
    .then(() => {
        console.log(SCHEDULER_STARTED);
    })
    .catch(console.error);
